//
//  Defect+Helper.m
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "Defect+Helper.h"
#import "AppDelegate.h"
#import "Gemag.conf.h"
#import "CustomNSManagedObjectContext.h"

@implementation Defect (Helper)


+ (NSMutableArray *) getDefectsWithFilter:(NSDictionary*) filter andOrder:(NSString *) order andAscending:(BOOL) ascending {
    NSPredicate *predSearch;
    if (!filter) {
        predSearch = nil;
    } else {
        predSearch = [NSPredicate predicateWithFormat:@"(%@ == %@)",[filter objectForKey:@"field"] , [filter objectForKey:@"value"]];
    }
    
    DLog(@"Core Data - Defect : %@", [CoreDataHelper searchObjectsForEntity:@"Defect" withPredicate:predSearch andSortKey:order andSortAscending:ascending andContext:[AppDelegate getContext]]);
    return [CoreDataHelper searchObjectsForEntity:@"Defect" withPredicate:predSearch andSortKey:order andSortAscending:ascending andContext:[AppDelegate getContext]];
}


+ (void) populateDefectTable {
    
    [CoreDataHelper deleteAllObjectsForEntity:@"Defect" andContext:[[AppDelegate getDelegate] managedObjectContext]];
    
    Defect *defect;
    
    defect = (Defect *)[NSEntityDescription insertNewObjectForEntityForName:@"Defect" inManagedObjectContext:[AppDelegate getContext]];
    [defect setRef:@""];
    [defect setTitle:@"Problème electrique"];
    [defect setDesc:@"Défaut electrique dans le local technique du bat D. Disjoncteur défecteux à changer. 16A - 230V"];
    [defect setDate:[NSDate date]];
    [defect setAuthor:@"Sylvain Tudela"];
    [defect setPriority:@(DEFECT_PRIORITY_HIGH)];
    [defect setState:@(DEFECT_STATE_INCORRECTION)];
    [defect setLocationLat:@(43.609881)];
    [defect setLocationLong:@(7.019453)];
    [defect setLocationAlt:@(188.00)];
    [defect setLocationDesc:@"au RDC"];
    
    [[AppDelegate getDelegate] saveContext];
    
    defect = (Defect *)[NSEntityDescription insertNewObjectForEntityForName:@"Defect" inManagedObjectContext:[AppDelegate getContext]];
    [defect setRef:@""];
    [defect setTitle:@"Bureau défectueux"];
    [defect setDesc:@"Réparer le pied du bureau"];
    [defect setDate:[NSDate date]];
    [defect setAuthor:@"Sylvain Tudela"];
    [defect setPriority:@(DEFECT_PRIORITY_LOW)];
    [defect setState:@(DEFECT_STATE_NEW)];
    [defect setLocationLat:@(43.610062)];
    [defect setLocationLong:@(7.018716)];
    [defect setLocationAlt:@(192.00)];
    [defect setLocationDesc:@"au 1er étage"];
    
    [[AppDelegate getDelegate] saveContext];
    
    defect = (Defect *)[NSEntityDescription insertNewObjectForEntityForName:@"Defect" inManagedObjectContext:[AppDelegate getContext]];
    [defect setRef:@""];
    [defect setTitle:@"Défaut réseau"];
    [defect setDesc:@"Changer la prise murale RJ45 du bureau"];
    [defect setDate:[NSDate date]];
    [defect setAuthor:@"Guillaume Baranne"];
    [defect setPriority:@(DEFECT_PRIORITY_MEDIUM)];
    [defect setState:@(DEFECT_STATE_SCHEDULED)];
    [defect setLocationLat:@(43.609579)];
    [defect setLocationLong:@(7.019373)];
    [defect setLocationAlt:@(195.00)];
    [defect setLocationDesc:@"au 2eme étage"];
    
    [[AppDelegate getDelegate] saveContext];
    
    defect = (Defect *)[NSEntityDescription insertNewObjectForEntityForName:@"Defect" inManagedObjectContext:[AppDelegate getContext]];
    [defect setRef:@""];
    [defect setTitle:@"Peinture parking"];
    [defect setDesc:@"Repeindre les place de parking au sol"];
    [defect setDate:[NSDate date]];
    [defect setAuthor:@"Guillaume Baranne"];
    [defect setPriority:@(DEFECT_PRIORITY_CRITICAL)];
    [defect setState:@(DEFECT_STATE_TERMINATED)];
    [defect setLocationLat:@(43.610869)];
    [defect setLocationLong:@(7.017364)];
    [defect setLocationAlt:@(195.00)];
    [defect setLocationDesc:@"à côté de la jardinière"];
    
    [[AppDelegate getDelegate] saveContext];
    
    defect = (Defect *)[NSEntityDescription insertNewObjectForEntityForName:@"Defect" inManagedObjectContext:[AppDelegate getContext]];
    [defect setRef:@""];
    [defect setTitle:@"Peinture parking"];
    [defect setDesc:@"Repeindre les place de parking au sol"];
    [defect setDate:[NSDate date]];
    [defect setAuthor:@"Michael Mousset"];
    [defect setPriority:@(DEFECT_PRIORITY_MEDIUM)];
    [defect setState:@(DEFECT_STATE_PENDING)];
    [defect setLocationLat:@(43.609341)];
    [defect setLocationLong:@(7.020328)];
    [defect setLocationAlt:@(190.00)];
    [defect setLocationDesc:@"au niveau de sentier qui part en direction de l'étant"];
    
    [[AppDelegate getDelegate] saveContext];
    
    defect = (Defect *)[NSEntityDescription insertNewObjectForEntityForName:@"Defect" inManagedObjectContext:[AppDelegate getContext]];
    [defect setRef:@""];
    [defect setTitle:@"Signalement Marco Polo"];
    [defect setDesc:@"Ajouter un panneau de signalement indiquant le Marco Polo au niveau du départ du chemin"];
    [defect setDate:[NSDate date]];
    [defect setAuthor:@"Michael Mousset"];
    [defect setPriority:@(DEFECT_PRIORITY_LOW)];
    [defect setState:@(DEFECT_STATE_NEW)];
    [defect setLocationLat:@(43.610281)];
    [defect setLocationLong:@(7.020298)];
    [defect setLocationAlt:@(198.00)];
    [defect setLocationDesc:@"au départ du racourci au niveau de la route"];
    
    [[AppDelegate getDelegate] saveContext];
    
    defect = (Defect *)[NSEntityDescription insertNewObjectForEntityForName:@"Defect" inManagedObjectContext:[AppDelegate getContext]];
    [defect setRef:@""];
    [defect setTitle:@"Remplacer le poste informatique"];
    [defect setDesc:@"Poste informatique défaillant."];
    [defect setDate:[NSDate date]];
    [defect setAuthor:@"Laurent Perez"];
    [defect setPriority:@(DEFECT_PRIORITY_LOW)];
    [defect setState:@(DEFECT_STATE_NEW)];
    [defect setLocationLat:@(43.625161)];
    [defect setLocationLong:@(7.039451)];
    [defect setLocationAlt:@(198.00)];
    [defect setLocationDesc:@"à l'agence"];
    
    [[AppDelegate getDelegate] saveContext];
}
@end
