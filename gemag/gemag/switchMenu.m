//
//  switchMenu.m
//  gemag
//
//  Created by Guillaume BARANNE on 23/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "switchMenu.h"

@implementation switchMenu


-(id) init
{
    self = [super init];
    if(self)
    {
        
      //hkqjshd
        
        }
    return self;
}

- (AwesomeMenu*) getMenu
{
    UIImage *storyMenuItemImage = [UIImage imageNamed:@"bg-menuitem.png"];
    UIImage *storyMenuItemImagePressed = [UIImage imageNamed:@"bg-menuitem-highlighted.png"];
    //UIImage *starImage = [UIImage imageNamed:@"icon-star.png"];
    UIImage *listImage = [UIImage imageNamed:@"list-vew_13.png"];
    UIImage *locationImage = [UIImage imageNamed:@"location-pin_20.png"];
    UIImage *locationPointerImage = [UIImage imageNamed:@"location-pointer_20.png"];
    
    AwesomeMenuItem *starMenuItem1 = [[AwesomeMenuItem alloc] initWithImage:storyMenuItemImage
                                                           highlightedImage:storyMenuItemImagePressed
                                                               ContentImage:listImage
                                                    highlightedContentImage:nil];
    
    AwesomeMenuItem *starMenuItem2 = [[AwesomeMenuItem alloc] initWithImage:storyMenuItemImage
                                                           highlightedImage:storyMenuItemImagePressed
                                                               ContentImage:locationImage
                                                    highlightedContentImage:nil];
    AwesomeMenuItem *starMenuItem3 = [[AwesomeMenuItem alloc] initWithImage:storyMenuItemImage
                                                           highlightedImage:storyMenuItemImagePressed
                                                               ContentImage:locationPointerImage
                                                    highlightedContentImage:nil];
    
    
    NSArray *menus = [NSArray arrayWithObjects:starMenuItem1, starMenuItem2, starMenuItem3, nil];
    
    AwesomeMenuItem *startItem = [[AwesomeMenuItem alloc] initWithImage:[UIImage imageNamed:@"bg-addbutton.png"]
                                                       highlightedImage:[UIImage imageNamed:@"bg-addbutton-highlighted.png"]
                                                           ContentImage:[UIImage imageNamed:@"0219.png"]
                                                highlightedContentImage:[UIImage imageNamed:@"0219.png"]];
    
    AwesomeMenu *menu = [[AwesomeMenu alloc]  initWithFrame:self.menuFrame startItem:startItem optionMenus:menus];
    // self.menu = menu; // desole c'est un peu crade... (a revoir pour que cette view soit accessible dans la methode scrollViewDidScroll
    
    //menu.delegate = self;
    menu.menuWholeAngle = M_PI_2;
    menu.farRadius = 110.0f;
    menu.endRadius = 100.0f;
    menu.nearRadius = 90.0f;
    menu.animationDuration = 0.3f;
    menu.startPoint = CGPointMake(25.0, 390.0); // coordonnees du bouton add
    
    return menu;
}






@end
