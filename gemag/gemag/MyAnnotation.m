//
//  MyAnnotation.m
//  gemag
//
//  Created by Guillaume BARANNE on 21/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
