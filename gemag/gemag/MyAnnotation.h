//
//  MyAnnotation.h
//  gemag
//
//  Created by Guillaume BARANNE on 21/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MyAnnotation : MKAnnotationView

@end
