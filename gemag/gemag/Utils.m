//
//  Utils.m
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (id)getInstance
{
    static dispatch_once_t pred = 0;
    __strong static id utils = nil;
    dispatch_once(&pred, ^{
        utils = [[self alloc] init]; // or some other init method
    });
    return utils;
}

+(UIImage*)drawImageOfSize:(CGSize)size andColor:(UIColor*)color{
    
    UIGraphicsBeginImageContext(size);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGRect fillRect = CGRectMake(0,0,size.width,size.height);
    CGContextSetFillColorWithColor(currentContext, color.CGColor);
    CGContextFillRect(currentContext, fillRect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}

+(UIImage*)drawRoundedImageOfSize:(CGSize)size andColor:(UIColor*)color{
    
    
    
    CGRect rect = CGRectMake(0,0,size.width,size.height);
    CGFloat radius = 3.0f;
    
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    
    CGContextMoveToPoint(context, rect.origin.x, rect.origin.y + radius);
    CGContextAddLineToPoint(context, rect.origin.x, rect.origin.y + rect.size.height - radius);
    CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + rect.size.height - radius,
                    radius, M_PI, M_PI / 2, 1); //STS fixed
    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width - radius,
                            rect.origin.y + rect.size.height);
    CGContextAddArc(context, rect.origin.x + rect.size.width - radius,
                    rect.origin.y + rect.size.height - radius, radius, M_PI / 2, 0.0f, 1);
    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width, rect.origin.y + radius);
    CGContextAddArc(context, rect.origin.x + rect.size.width - radius, rect.origin.y + radius,
                    radius, 0.0f, -M_PI / 2, 1);
    CGContextAddLineToPoint(context, rect.origin.x + radius, rect.origin.y);
    CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + radius, radius,
                    -M_PI / 2, M_PI, 1);
    
    CGContextFillPath(context);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}

+(UIBarButtonItem*)getSearchButtonItem{
    
    UIImageView* searchView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search.png"]];
    searchView.frame = CGRectMake(0, 0, 20, 20);
    
    UIBarButtonItem* searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchView];
    
    return searchItem;
    
}

+(UIBarButtonItem*)getMenuItem{
    UIImageView* menuView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu.png"]];
    menuView.frame = CGRectMake(0, 0, 28, 20);
    
    UIBarButtonItem* menuItem = [[UIBarButtonItem alloc] initWithCustomView:menuView];
    
    return menuItem;
}

- (NSString *) getCustomDateFormatForDate:(NSDate *)date {
    if(!self.formatters){
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setTimeStyle:NSDateFormatterShortStyle];
        [timeFormatter setDateStyle:NSDateFormatterNoStyle];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        
        NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
        [dayFormatter setDateFormat:@"ccc"];
        
        NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc] init];
        [shortDateFormatter setDateFormat:@"dd'/'MM"];
        
        [self setFormatters: [NSArray arrayWithObjects:dateFormatter, timeFormatter, dayFormatter/*, monthFormatter*/, shortDateFormatter, nil]];
        
        self.gregorianCal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    }
    
    NSDate *now = [[NSDate alloc] init];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSWeekCalendarUnit | NSDayCalendarUnit;
    NSDateComponents* comp1 = [self.gregorianCal components:unitFlags fromDate:now];
    NSDateComponents* comp2 = [self.gregorianCal components:unitFlags fromDate:date];
    
    NSUInteger dayOfYearFromNow = [self.gregorianCal ordinalityOfUnit:NSDayCalendarUnit
                                                               inUnit:NSYearCalendarUnit forDate:now];
    NSUInteger dayOfYearFromDate = [self.gregorianCal ordinalityOfUnit:NSDayCalendarUnit
                                                                inUnit:NSYearCalendarUnit forDate:date];
    
    NSDateFormatter *returnedFormat = nil;
    
    // TODO : perform a string comparison (lighter)
    if ([comp1 day]   == [comp2 day] &&
        [comp1 month] == [comp2 month] &&
        [comp1 year]  == [comp2 year])
    {
        returnedFormat = [self.formatters objectAtIndex:1];
    }
    // Last seven days : display day name
    else if ([comp1 year] == [comp2 year] && (dayOfYearFromNow - dayOfYearFromDate) < 7 ){
        returnedFormat = [self.formatters objectAtIndex:2];
    }
    /*    else if ([comp1 year] == [comp2 year] &&
     [comp1 week] - [comp2 week] < 4){
     returnedFormat = [self.formatters objectAtIndex:4];
     }
     */
    else if ([comp1 year] == [comp2 year]){
        returnedFormat = [self.formatters objectAtIndex:3];
    }
    else {
        returnedFormat = [self.formatters objectAtIndex:0];
    }
    
    
    return [returnedFormat stringFromDate:date];
}


@end
