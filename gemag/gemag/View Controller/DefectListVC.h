//
//  MainViewController.h
//  ADVFlatUI
//
//  Created by Tope on 05/06/2013.
//  Copyright (c) 2013 App Design Vault. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AwesomeMenu.h"
#import <CoreLocation/CoreLocation.h>

@interface DefectListVC : UIViewController <UITableViewDataSource, UITableViewDelegate, AwesomeMenuDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, weak) IBOutlet UITableView* defectTableView;
@property (nonatomic, strong) NSMutableArray *defectList;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *myLocation;

@property (nonatomic, weak) AwesomeMenu *menu;
@end
