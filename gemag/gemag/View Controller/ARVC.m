//
//  ARVC.m
//  gemag
//
//  Created by Guillaume BARANNE on 14/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "ARVC.h"
#import "switchMenu.h"

@interface ARVC ()

@end

@implementation ARVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    switchMenu *menugbe = [[switchMenu alloc] init];
    self.menu = [menugbe getMenu];
    
    self.menu.delegate = self;
    
    [self.view addSubview:self.menu];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* ⬇⬇⬇⬇⬇⬇ GET RESPONSE OF MENU ⬇⬇⬇⬇⬇⬇ */
- (void)awesomeMenu:(AwesomeMenu *)menu didSelectIndex:(NSInteger)idx
{
    NSLog(@"Select the index : %d",idx);
    
    NSDictionary* idMenu = [NSDictionary dictionaryWithObject:[[NSString alloc] initWithFormat:@"%d", idx] forKey:@"idMenu"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeDefectView" object:self userInfo:idMenu];
    
}

@end
