//
//  MapVC.h
//  gemag
//
//  Created by Guillaume BARANNE on 14/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REVClusterMapView.h"
#import "AwesomeMenu.h"

@interface MapVC : UIViewController <MKMapViewDelegate, AwesomeMenuDelegate> {
    MKMapView *_mapView;
    NSManagedObjectContext *managedObjectContext;
    }
@property (nonatomic, strong) NSMutableArray *defectList;



@property (nonatomic, retain) IBOutlet MKMapView *_mapView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, weak) AwesomeMenu *menu;
- (IBAction)closeMap:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *masterView;

@end
