//
//  LoginController2.m
//  ADVFlatUI
//
//  Created by Tope on 30/05/2013.
//  Copyright (c) 2013 App Design Vault. All rights reserved.
//

#import "LoginVC.h"
#import "Authent.h"
#import "TSMessage.h"
#import <QuartzCore/QuartzCore.h>


@interface LoginVC () 

@end

@implementation LoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
    // Initialisation graphique de la vue
    UIColor* mainColor = [UIColor colorWithRed:222.0/255 green:59.0/255 blue:47.0/255 alpha:1.0f];
    UIColor* darkColor = [UIColor colorWithRed:135.0/255 green:34.0/255 blue:26.0/255 alpha:1.0f];
    
    NSString* fontName = @"Avenir-Book";
    NSString* boldFontName = @"Avenir-Black";
    
    self.view.backgroundColor = mainColor;
    
    self.usernameField.backgroundColor = [UIColor whiteColor];
    self.usernameField.layer.cornerRadius = 3.0f;
    self.usernameField.placeholder = NSLocalizedString(@"LOG_IDENTIFIER", @"");
    self.usernameField.leftViewMode = UITextFieldViewModeAlways;
    UIView* leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    self.usernameField.leftView = leftView1;
    self.usernameField.font = [UIFont fontWithName:fontName size:16.0f];
    
    self.passwordField.backgroundColor = [UIColor whiteColor];
    self.passwordField.layer.cornerRadius = 3.0f;
    self.passwordField.placeholder = NSLocalizedString(@"LOG_PASSWORD", @"");
    self.passwordField.leftViewMode = UITextFieldViewModeAlways;
    UIView* leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    self.passwordField.leftView = leftView2;
    self.passwordField.font = [UIFont fontWithName:fontName size:16.0f];
    
    self.loginButton.backgroundColor = darkColor;
    self.loginButton.layer.cornerRadius = 3.0f;
    self.loginButton.titleLabel.font = [UIFont fontWithName:boldFontName size:20.0f];
    [self.loginButton setTitle:NSLocalizedString(@"LOG_BUTTON", @"") forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    
    self.forgotButton.backgroundColor = [UIColor clearColor];
    self.forgotButton.titleLabel.font = [UIFont fontWithName:fontName size:12.0f];
    [self.forgotButton setTitle:NSLocalizedString(@"LOG_FORGOT", @"") forState:UIControlStateNormal];
    [self.forgotButton setTitleColor:darkColor forState:UIControlStateNormal];
    [self.forgotButton setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
    
    self.titleLabel.textColor =  [UIColor whiteColor];
    self.titleLabel.font =  [UIFont fontWithName:boldFontName size:24.0f];
    self.titleLabel.text = NSLocalizedString(@"APP_TITLE", @"");
    
    self.subTitleLabel.textColor =  [UIColor whiteColor];
    self.subTitleLabel.font =  [UIFont fontWithName:fontName size:14.0f];
    self.subTitleLabel.text = NSLocalizedString(@"APP_SUBTITLE", @"");
    
    DismissKeyboardAccessoryView *dismissKeyboardButton = [[DismissKeyboardAccessoryView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.bounds.size.width, 30)];
    dismissKeyboardButton.delegate = self;
    self.usernameField.inputAccessoryView = dismissKeyboardButton;
    self.passwordField.inputAccessoryView = dismissKeyboardButton;
    
    self.usernameField.returnKeyType = UIReturnKeyDone;
    self.usernameField.delegate = self;
    
    self.passwordField.returnKeyType = UIReturnKeyDone;
    self.passwordField.delegate = self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardDismissButtonTapped {
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self keyboardDismissButtonTapped];
    return YES;
}

- (IBAction)launchAuthentication:(id)sender {
    [self keyboardDismissButtonTapped];
    if ([self.usernameField.text isEqualToString:@"demo"] && [self.passwordField.text isEqualToString:@"demo"]) {
        // redirection vers la liste des defects
        [[Authent getInstance] setLogin:@"demo"];
        [[Authent getInstance] setPassword:@"demo"];
        
        [[Authent getInstance] setFirstName:@"Sylvain"];
        [[Authent getInstance] setLastName:@"Tudela"];
        [[Authent getInstance] setCompany:@"Sopra Group"];
        [[Authent getInstance] setEmail:@"studela@sopragroup.com"];
        [[Authent getInstance] setPhone:@"+33 6 09 00 00 00"];
        
        [[Authent getInstance] setIsAuthent:YES];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        // affichage d'une alert
        
        [TSMessage showNotificationInViewController:self
                                          withTitle:@"Authentification invalide"
                                        withMessage:@"Identifiant ou mot de passe invalide."
                                           withType:TSMessageNotificationTypeError
                                       withDuration:3.0
                                       withCallback:nil];
        
        
    }
    
}
@end

