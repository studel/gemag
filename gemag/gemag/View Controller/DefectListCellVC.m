//
//  DefectMainCellVC.m
//
//  Created by Tope on 03/06/2013.
//  Copyright (c) 2013 App Design Vault. All rights reserved.
//

#import "DefectListCellVC.h"
#import "UIColor+MLPFlatColors.h"
#import <QuartzCore/QuartzCore.h>

@implementation DefectListCellVC

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)awakeFromNib{
    
    self.bgView.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
    
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = self.bgView.bounds;
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:239.0/255.0 green:238.0/255.0 blue:239.0/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0] CGColor], nil];
    
    [self.bgView.layer insertSublayer:grad atIndex:0];
    
    CAGradientLayer *selectedGrad = [CAGradientLayer layer];
    selectedGrad.frame = self.bgView.bounds;
    selectedGrad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:239.0/255.0 green:238.0/255.0 blue:239.0/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0] CGColor], nil];
    
    [self.bgView.layer insertSublayer:selectedGrad atIndex:0];
    
    
    
    self.topSeparator.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:250.0/255.0 alpha:1.0];
    self.bottomSeparator.backgroundColor = [UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0];
    
    UIColor* mainColor = [UIColor colorWithRed:135.0/255 green:34.0/255 blue:26.0/255 alpha:1.0f];
    UIColor* neutralColor = [UIColor colorWithWhite:0.4 alpha:1.0];
    
    UIColor* mainColorLight = [UIColor colorWithRed:222.0/255 green:59.0/255 blue:47.0/255 alpha:1.0f];
    UIColor* lightColor = [UIColor colorWithWhite:0.7 alpha:1.0];
    
    NSString* fontName = @"Avenir-Book";
    NSString* boldItalicFontName = @"Avenir-BlackOblique";
    NSString* boldFontName = @"Avenir-Black";
    
    self.nameLabel.textColor =  mainColor;
    self.nameLabel.font =  [UIFont fontWithName:boldFontName size:14.0f];
    
    self.updateLabel.textColor =  neutralColor;
    self.updateLabel.font =  [UIFont fontWithName:fontName size:12.0f];
    
    self.dateLabel.textColor = lightColor;
    self.dateLabel.font =  [UIFont fontWithName:boldItalicFontName size:8.0f];
    
    self.commentCountLabel.textColor = mainColorLight;
    self.commentCountLabel.font =  [UIFont fontWithName:boldItalicFontName size:10.0f];
    
    self.likeCountLabel.textColor = mainColorLight;
    self.likeCountLabel.font =  [UIFont fontWithName:boldItalicFontName size:10.0f];
    
    self.picImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.picImageView.clipsToBounds = YES;
    self.picImageView.layer.cornerRadius = 2.0f;
    
    self.picImageContainer.backgroundColor = [UIColor whiteColor];
    self.picImageContainer.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:0.6f].CGColor;
    self.picImageContainer.layer.borderWidth = 1.0f;
    self.picImageContainer.layer.cornerRadius = 2.0f;
    
    //self.profileImageView.image = [UIImage imageNamed:@"sopragroup.png"];
    //self.profileImageView.clipsToBounds = YES;
    //self.profileImageView.layer.borderWidth = 4.0f;
    //self.profileImageView.layer.borderColor = [UIColor colorWithWhite:1.0f alpha:0.5f].CGColor;
    //self.profileImageView.layer.cornerRadius = 35.0f;
        
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
