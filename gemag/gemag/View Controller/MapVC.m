//
//  MapVC.m
//  gemag
//
//  Created by Guillaume BARANNE on 14/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "MapVC.h"
#import "REVClusterMap.h"
#import "REVClusterAnnotationView.h"
#import "Defect.h"
#import "Defect+Helper.h"
#import "CoreDataHelper.h"
#import "AppDelegate.h"
#import "switchMenu.h"


#define BASE_RADIUS .5 // = 1 mile
#define MINIMUM_LATITUDE_DELTA 0.20
#define BLOCKS 4

#define MINIMUM_ZOOM_LEVEL 100000

@interface MapVC ()

@end

@implementation MapVC
@synthesize _mapView, managedObjectContext;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.defectList = [[NSMutableArray alloc] init];
   
     
    _mapView.showsUserLocation = YES;
    
}


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    [super loadView];
    
    //CGRect viewBounds = [[UIScreen mainScreen] applicationFrame];
    CGRect viewBounds = self.masterView.frame;
    _mapView = [[REVClusterMapView alloc] initWithFrame:viewBounds];
    
    _mapView.delegate = self;

    
    [self.masterView addSubview:_mapView];
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 43.771094;
    coordinate.longitude = 7.195616;
    
    
    _mapView.region = MKCoordinateRegionMakeWithDistance(coordinate, 50000, 50000);
    
    NSMutableArray *pins = [NSMutableArray array];
    
    
    self.managedObjectContext = [AppDelegate getContext];

    NSError *error;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Defect" inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *info in fetchedObjects) {

        CLLocationCoordinate2D newCoord = {[[info valueForKey:@"locationLat"] floatValue], [[info valueForKey:@"locationLong"] floatValue]};
        REVClusterPin *pin = [[REVClusterPin alloc] init];
        pin.title = [NSString stringWithFormat:@"%@",[info valueForKey:@"author"]];
        pin.subtitle = [NSString stringWithFormat:@"%@",[info valueForKey:@"desc"]];
        pin.coordinate = newCoord;
        [pins addObject:pin];
        
        }
 
    [_mapView addAnnotations:pins];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    switchMenu *menugbe = [[switchMenu alloc] init];
    self.menu = [menugbe getMenu];
    
    self.menu.delegate = self;
    
    [self.masterView addSubview:self.menu];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}*/

- (IBAction)closeMap:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

#pragma mark -
#pragma mark Map view delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if([annotation class] == MKUserLocation.class) {
		//userLocation = annotation;
		return nil;
	}
    
    REVClusterPin *pin = (REVClusterPin *)annotation;
    
    MKAnnotationView *annView;
    
    if( [pin nodeCount] > 0 ){
        pin.title = @"___";
        
        annView = (REVClusterAnnotationView*)
        [_mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
        
        if( !annView )
            annView = (REVClusterAnnotationView*)
            [[REVClusterAnnotationView alloc] initWithAnnotation:annotation
                                                  reuseIdentifier:@"cluster"];
        
        annView.image = [UIImage imageNamed:@"cluster.png"];
        
        [(REVClusterAnnotationView*)annView setClusterText:
         [NSString stringWithFormat:@"%i",[pin nodeCount]]];
        
        annView.canShowCallout = NO;
    } else {
        annView = [_mapView dequeueReusableAnnotationViewWithIdentifier:@"pin"];
        
        if( !annView )
            annView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                    reuseIdentifier:@"pin"];
        
        annView.image = [UIImage imageNamed:@"pinpoint.png"];
        annView.canShowCallout = YES;
        
        annView.calloutOffset = CGPointMake(-6.0, 0.0);
    }
    return annView;
}

- (void)mapView:(MKMapView *)mapView
didSelectAnnotationView:(MKAnnotationView *)view
{
    NSLog(@"REVMapViewController mapView didSelectAnnotationView:");
    
    if (![view isKindOfClass:[REVClusterAnnotationView class]])
        return;
    
    CLLocationCoordinate2D centerCoordinate = [(REVClusterPin *)view.annotation coordinate];
    
    MKCoordinateSpan newSpan =
    MKCoordinateSpanMake(_mapView.region.span.latitudeDelta/2.0,
                         _mapView.region.span.longitudeDelta/2.0);
    
    //mapView.region = MKCoordinateRegionMake(centerCoordinate, newSpan);
    
    [_mapView setRegion:MKCoordinateRegionMake(centerCoordinate, newSpan)
              animated:YES];
}


/* ⬇⬇⬇⬇⬇⬇ GET RESPONSE OF MENU ⬇⬇⬇⬇⬇⬇ */
- (void)awesomeMenu:(AwesomeMenu *)menu didSelectIndex:(NSInteger)idx
{
    NSLog(@"Select the index : %d",idx);
    
    NSDictionary* idMenu = [NSDictionary dictionaryWithObject:[[NSString alloc] initWithFormat:@"%d", idx] forKey:@"idMenu"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeDefectView" object:self userInfo:idMenu];
        
}

@end
