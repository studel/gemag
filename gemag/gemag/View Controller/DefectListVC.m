//
//  MainViewController.m
//  ADVFlatUI
//
//  Created by Tope on 05/06/2013.
//  Copyright (c) 2013 App Design Vault. All rights reserved.
//

#import "DefectListVC.h"
#import "DefectListCellVC.h"
#import "UIColor+MLPFlatColors.h"
#import "Defect.h"
#import "Defect+Helper.h"
#import "AppDelegate.h"
#import "Utils.h"
#import "switchMenu.h"


@interface DefectListVC ()

@end

@implementation DefectListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.defectTableView.dataSource = self;
    self.defectTableView.delegate = self;
    self.defectTableView.backgroundColor = [UIColor whiteColor];
    self.defectTableView.separatorColor = [UIColor colorWithWhite:0.9 alpha:0.6];
    self.defectList = [[NSMutableArray alloc] init];
        

    
    switchMenu *menugbe = [[switchMenu alloc] init];
    self.menu = [menugbe getMenu];

    self.menu.delegate = self;

    [self.defectTableView addSubview:self.menu];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeOrderFilterDelegate:) name:@"changeOrderFilterDelegate" object:nil];
    
}

- (void)viewDidUnload {
    [super viewDidUnload];    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self readDataForTable:nil andOrder:@"date" andAsc:NO];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation; // 100 m
    [self.locationManager startUpdatingLocation];
    
    float myLat = self.locationManager.location.coordinate.latitude;
    float myLon = self.locationManager.location.coordinate.longitude;
    float myAlt = self.locationManager.location.altitude;
    
    DLog(@"MyLat : %f - myLon : %f", myLat, myLon);
    CLLocationCoordinate2D Coord = CLLocationCoordinate2DMake(myLat, myLon);
    self.myLocation = [[CLLocation alloc] initWithCoordinate: Coord altitude:myAlt horizontalAccuracy:1 verticalAccuracy:-1 timestamp:nil];
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    DLog(@"NB Cell : %d", [self.defectList count]);
    return [self.defectList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DefectListCellVC* cell = [tableView dequeueReusableCellWithIdentifier:@"DefectListCell"];
    Defect *defect = [self.defectList objectAtIndex:indexPath.row];
    
    // TITLE
    cell.nameLabel.text = [defect title];
    
    // DESCRIPTION
    cell.updateLabel.text = [defect desc];
    
    // DATE
    cell.dateLabel.text = [[Utils getInstance] getCustomDateFormatForDate:[defect date]];
    
    // AUTHOR
    cell.likeCountLabel.text = [defect author];
    
    // DISTANCE
    
    DLog(@"Defect %@, lat : %f, lon, %f", [defect title], [[defect locationLat] floatValue], [[defect locationLong] floatValue]);
    CLLocationCoordinate2D Coord = CLLocationCoordinate2DMake([[defect locationLat] floatValue], [[defect locationLong] floatValue]);
    CLLocation *defectLocation = [[CLLocation alloc] initWithCoordinate:Coord altitude:[[defect locationAlt] floatValue] horizontalAccuracy:1 verticalAccuracy:-1 timestamp:nil];
    CLLocationDistance meters = [defectLocation distanceFromLocation:self.myLocation];
    int roundMeters = meters;
    
    cell.commentCountLabel.text = [self formatDistance:roundMeters];
    
    // DEFECT STATUS
    // New -> flatWhiteColor
    // Scheduled -> flatDarkWhiteColor
    // In correction -> flatDarkYellowColor
    // Terminated -> flatDarkGreenColor
    // Pending -> flatRedColor
    DEFECT_STATE state = [[defect state] intValue];
    switch (state)
    {
        case DEFECT_STATE_NEW:
            cell.statusView.backgroundColor = [UIColor flatWhiteColor];
            break;
        case DEFECT_STATE_SCHEDULED:
            cell.statusView.backgroundColor = [UIColor flatDarkWhiteColor];
            break;
        case DEFECT_STATE_INCORRECTION:
            cell.statusView.backgroundColor = [UIColor flatDarkYellowColor];
            break;
        case DEFECT_STATE_TERMINATED:
            cell.statusView.backgroundColor = [UIColor flatDarkGreenColor];
            break;
        case DEFECT_STATE_PENDING:
            cell.statusView.backgroundColor = [UIColor flatRedColor];
            break;
        default:
            cell.statusView.backgroundColor = [UIColor flatWhiteColor];            
            break;
    }
        
    // DEFECT LEVEL
    // Low
    // Medium
    // High
    // Critical
    NSString* defectImageName;
    DEFECT_PRIORITY priority = [[defect priority] intValue];
    switch (priority)
    {
        case DEFECT_PRIORITY_LOW:
            defectImageName = @"level-law.png";
            break;
        case DEFECT_PRIORITY_MEDIUM:
            defectImageName = @"level-medium.png";
            break;
        case DEFECT_PRIORITY_HIGH:
            defectImageName = @"level-high.png";
            break;
        case DEFECT_PRIORITY_CRITICAL:
            defectImageName = @"level-critical.png";
            break;
        default:
            defectImageName = @"level-law.png";
            break;
    }
    cell.profileImageView.image = [UIImage imageNamed:defectImageName];
    
    return cell;
}

// Force le reload la liste des comptes
- (void)readDataForTable: (NSDictionary *) filter andOrder: (NSString *) order andAsc : (BOOL) asc {
//    NSDictionary *filter = [[NSDictionary alloc] initWithObjectsAndKeys:
//                         @"field", @"author", @"value",
//                         @"Sylvain Tudela", nil];
    
    //Vidage de la liste
    [self.defectList removeAllObjects];
    //Charge tous les utilisateurs de la BDD
    [self.defectList addObjectsFromArray:[Defect getDefectsWithFilter:filter andOrder:order andAscending:asc]];
        
    //Force le chargement des nouvelles données dans la liste
    [self.defectTableView reloadData];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGRect tableBounds = self.defectTableView.bounds; // gets content offset
    CGRect frameForStillView =  self.menu.frame;
    frameForStillView.origin.y = tableBounds.origin.y; // offsets the rects y origin by the content offset
    self.menu.frame = frameForStillView; // set the frame to the new calculation
}


- (NSString*) formatDistance: (int) distance; {
    if (distance > 1000) {
        float km = distance;
        return [NSString stringWithFormat:@"%.2f km", (km / 1000)];
    } else {
        return [NSString stringWithFormat:@"%d m", distance];
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation: (CLLocation*)newLocation fromLocation:(CLLocation *)oldLocation {
    
    float myLat = newLocation.coordinate.latitude;
    float myLon = newLocation.coordinate.longitude;
    float myAlt = newLocation.altitude;    
    CLLocationCoordinate2D Coord = CLLocationCoordinate2DMake(myLat, myLon);
    self.myLocation = [[CLLocation alloc] initWithCoordinate: Coord altitude:myAlt horizontalAccuracy:1 verticalAccuracy:-1 timestamp:nil];
    
    [self.defectTableView reloadData];
}





// Delegate appelé depuis le menu
- (void)changeOrderFilterDelegate:(NSNotification *)notif{
    //Mise ajour de la liste des comptes
    NSString *tri = [notif.userInfo objectForKey:@"tri"];
    BOOL sort;
    if ([[notif.userInfo objectForKey:@"sort"] isEqualToString:@"YES"]) {
        sort = YES;
    } else {
        sort = NO;
    }
    

    if ([tri isEqualToString:@"distance"]){
        tri = nil;
    }
    [self readDataForTable:nil andOrder:tri andAsc:sort];
}

/* ⬇⬇⬇⬇⬇⬇ GET RESPONSE OF MENU ⬇⬇⬇⬇⬇⬇ */
- (void)awesomeMenu:(AwesomeMenu *)menu didSelectIndex:(NSInteger)idx
{
    NSLog(@"Select the index : %d",idx);
        
    // Notification
    NSDictionary* idMenu = [NSDictionary dictionaryWithObject:[[NSString alloc] initWithFormat:@"%d", idx] forKey:@"idMenu"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeDefectView" object:self userInfo:idMenu];
}

@end

