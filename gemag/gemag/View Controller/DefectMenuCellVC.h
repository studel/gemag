//
//  DefectCellVC.h
//

#import <UIKit/UIKit.h>
#include "PickerInputTableViewCell.h"

@class DefectMenuCellVC;

@protocol DefectMenuCellVCDelegate <NSObject>
@optional
- (void)tableViewCell:(PickerInputTableViewCell *)cell didEndEditingWithValue:(NSString *)value;
@end

@interface DefectMenuCellVC : PickerInputTableViewCell <UIPickerViewDataSource, UIPickerViewDelegate> 

@property (nonatomic, weak) IBOutlet UILabel* titleLabel;

@property (nonatomic, weak) IBOutlet UILabel* countLabel;

@property (nonatomic, weak) IBOutlet UIView* bgView;

@property (nonatomic, weak) IBOutlet UIView* topSeparator;

@property (nonatomic, weak) IBOutlet UIView* bottomSeparator;

@property (nonatomic, weak) IBOutlet UIImageView* iconImageView;

@property (nonatomic, strong) NSString *value;

@property (nonatomic, strong) NSArray *values;

@property (weak) IBOutlet id <DefectMenuCellVCDelegate> delegate;

@end
