//
//  LoginVC.h
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "DismissKeyboardAccessoryView.h"

@interface LoginVC : BaseVC <DismissKeyboardAccessoryViewDelegate, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField * usernameField;

@property (nonatomic, weak) IBOutlet UITextField * passwordField;

@property (nonatomic, weak) IBOutlet UIButton *loginButton;

@property (nonatomic, weak) IBOutlet UIButton * forgotButton;

@property (nonatomic, weak) IBOutlet UILabel * titleLabel;

@property (nonatomic, weak) IBOutlet UILabel * subTitleLabel;

- (IBAction)launchAuthentication:(id)sender;

@end
