//
//  DefectVC.m
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "DefectMenuVC.h"
#import "DefectMenuCellVC.h"
#import "Authent.h"
#import <QuartzCore/QuartzCore.h>

@interface DefectMenuVC ()

    @property (nonatomic, strong) NSArray* itemsTri;
    @property (nonatomic, assign) NSInteger triSelected;
    @property (nonatomic, strong) NSString *triSelectedString;
    @property (nonatomic, assign) NSString *triOrderString;
    @property (nonatomic, strong) NSArray* itemsFilter;

    @property (nonatomic, strong) NSArray* valueDate;
    @property (nonatomic, strong) NSArray* valueStatus;
    @property (nonatomic, strong) NSArray* valueDistance;
    @property (nonatomic, strong) NSArray* valuePriority;
    @property (nonatomic, strong) NSArray* valueAuthor;

    @property (nonatomic, strong) NSArray* valueAll;

@end

@implementation DefectMenuVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
    self.tableView.separatorColor = [UIColor clearColor];
    
    NSString* boldFontName = @"Avenir-Black";
    NSString* fontName = @"Avenir-BlackOblique";
    
    self.profileNameLabel.textColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
    self.profileNameLabel.font = [UIFont fontWithName:boldFontName size:14.0f];
    
    
    self.profileLocationLabel.textColor = [UIColor colorWithRed:222.0/255 green:59.0/255 blue:47.0/255 alpha:1.0f];
    self.profileLocationLabel.font = [UIFont fontWithName:fontName size:12.0f];
    
    
    self.profileImageView.image = [UIImage imageNamed:@"sopragroup.png"];
    self.profileImageView.clipsToBounds = YES;
    
    self.profileImageView.layer.borderWidth = 4.0f;
    self.profileImageView.layer.borderColor = [UIColor colorWithWhite:1.0f alpha:0.5f].CGColor;
    self.profileImageView.layer.cornerRadius = 32.0f;
    
    
    NSDictionary* tri1 = [NSDictionary dictionaryWithObjects:@[ @"Date", @"", @"0099", @"date" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    NSDictionary* tri2 = [NSDictionary dictionaryWithObjects:@[ @"Status", @"", @"0065", @"state" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    NSDictionary* tri3 = [NSDictionary dictionaryWithObjects:@[ @"Distance", @"", @"0063", @"distance" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    NSDictionary* tri4 = [NSDictionary dictionaryWithObjects:@[ @"Priority", @"", @"0222", @"priority" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    NSDictionary* tri5 = [NSDictionary dictionaryWithObjects:@[ @"Author", @"", @"0012", @"author" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    
    self.itemsTri = @[tri1, tri2, tri3, tri4, tri5];
    
    NSDictionary* filter1 = [NSDictionary dictionaryWithObjects:@[ @"Date", @"", @"0099", @"date" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    NSDictionary* filter2 = [NSDictionary dictionaryWithObjects:@[ @"Status", @"new", @"0065", @"state" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    NSDictionary* filter3 = [NSDictionary dictionaryWithObjects:@[ @"Distance", @"", @"0063", @"distance" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    NSDictionary* filter4 = [NSDictionary dictionaryWithObjects:@[ @"Priority", @"", @"0222", @"priority" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    NSDictionary* filter5 = [NSDictionary dictionaryWithObjects:@[ @"Author", @"", @"0012", @"author" ] forKeys:@[ @"title", @"count", @"icon", @"coredata" ]];
    
    self.itemsFilter = @[filter1, filter2, filter3, filter4, filter5];
    
    
    
    self.valueDate = [[NSArray alloc] initWithObjects:@"today", @"yesterday", @"this week", @"last week", @"this month", @"last month", nil];
    self.valueStatus = [[NSArray alloc] initWithObjects:@"new", @"scheduling", @"in correction", @"pending", @"terminated", nil];
    self.valueDistance = [[NSArray alloc] initWithObjects:@"< 100m", @"<250m", @"< 500m", @"< 1km", @"< 2,5km", @"< 5km", @"< 10 km", @"< 25km", @"< 50 km", @"< 100 km", nil];
    self.valuePriority = [[NSArray alloc] initWithObjects:@"low", @"medium", @"high", @"critical", nil];
    self.valueAuthor = [[NSArray alloc] initWithObjects:@"Sylvain Tudela", @"Guillaume Baranne", @"Michael Mousset", @"Laurent Perez", nil];
    
    self.valueAll = [[NSArray alloc] initWithObjects:self.valueDate, self.valueStatus, self.valueDistance, self.valuePriority, self.valueAuthor, nil];
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    self.profileNameLabel.text = [NSString stringWithFormat:@"%@ %@", [[Authent getInstance] firstName],  [[Authent getInstance] lastName]];
    self.profileLocationLabel.text = [[Authent getInstance] company];
}

// Nombre de cellule statique
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return [self.itemsTri count];
    } else {
        return [self.itemsFilter count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 25)];
    headerView.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1.0];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 200, 25)];
    label.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1.0];
    label.font = [UIFont fontWithName:@"Avenir-BlackOblique" size:16.0f];
    label.textColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
    
    label.text = section == 0 ? @"Tri" : @"Filtre";
    
    [headerView addSubview:label];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DefectMenuCellVC* cell = [tableView dequeueReusableCellWithIdentifier:@"DefectMenuCell"];
    NSDictionary* item;
    if (indexPath.section == 0){
        item = self.itemsTri[indexPath.row];
        
        if (indexPath.row == self.triSelected) {
            if ([self.triOrderString isEqualToString: @"YES"]){
                cell.countLabel.text = @"up";
            } else {
                cell.countLabel.text = @"down";
            }
            cell.countLabel.alpha = 1;
        } else {
            cell.countLabel.text = @"";
            cell.countLabel.alpha = 0;
        }
        
    } else {
        item = self.itemsFilter[indexPath.row];
                
        cell.values = [self.valueAll objectAtIndex:indexPath.row];
        cell.countLabel.text = @"";
        cell.countLabel.alpha = 0;
    }
    
    cell.titleLabel.text = item[@"title"];
    cell.iconImageView.image = [UIImage imageNamed:item[@"icon"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (indexPath.section == 0){
                
        if (indexPath.row == self.triSelected) {
            if ([self.triOrderString isEqualToString: @"YES"]){
                self.triOrderString = @"NO";
            } else {
                self.triOrderString = @"YES";
            }
        } else {
            self.triSelected = indexPath.row;
            NSDictionary* itemSel = self.itemsTri[indexPath.row];
            self.triSelectedString = [itemSel objectForKey:@"coredata"];

            self.triOrderString = @"NO";
        }
        
        
    
    } else if (indexPath.section == 1){
              
        NSArray *subOption = [self.valueAll objectAtIndex:indexPath.row];
        for ( NSString *anItem in subOption) {
            //
        }
        
    }
    
    [self.tableView reloadData];
    DLog(@"%@ - %@", self.triSelectedString, self.triOrderString);
    
    
    NSDictionary* item = [[NSDictionary alloc] initWithObjectsAndKeys:self.triSelectedString, @"field", self.triOrderString, @"sort", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeOrderFilterDelegate" object:self userInfo:item];
     
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
