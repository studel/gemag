//
//  DefectVC.h
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefectMenuCellVC.h"

@interface DefectMenuVC : UIViewController <UITableViewDataSource, UITableViewDelegate, DefectMenuCellVCDelegate>

@property (nonatomic, weak) IBOutlet UITableView* tableView;

@property (nonatomic, weak) IBOutlet UILabel* profileNameLabel;

@property (nonatomic, weak) IBOutlet UILabel* profileLocationLabel;

@property (nonatomic, weak) IBOutlet UIImageView* profileImageView;

@end
