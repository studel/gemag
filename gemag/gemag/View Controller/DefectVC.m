//
//  Defect.m
//  gemag
//
//  Created by Sylvain Tudela on 09/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "DefectVC.h"
#import "DefectListVC.h"
#import "DefectMenuVC.h"
#import "FlatTheme.h"
#import "Utils.h"
#import "Authent.h"

@interface DefectVC ()
    @property (nonatomic, strong) UIPanGestureRecognizer *panGesture;
@end

@implementation DefectVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(revealGesture:)];
    [self.panGesture setMaximumNumberOfTouches:1];
    [self.view addGestureRecognizer:self.panGesture];
    
    [self loadMasterView:@"DefectList"];
    
    // Register notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMasterViewNotification:) name:@"changeDefectView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toggleMenuDelegate:) name:@"toggleMenu" object:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    DLog(@"Is Authent : %d", [[Authent getInstance] isAuthent]);
    if ([[Authent getInstance] isAuthent] == NO) {
        [self performSegueWithIdentifier:@"DefectToLogin" sender:self];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadMasterViewNotification: (NSNotification *) notif {
    if ([[notif.userInfo objectForKey:@"idMenu"] isEqualToString:@"0" ]) {
        [self loadMasterView:@"DefectList"];
    } else if ([[notif.userInfo objectForKey:@"idMenu"] isEqualToString:@"1" ]) {
        [self loadMasterView:@"Map"];
    } else if ([[notif.userInfo objectForKey:@"idMenu"] isEqualToString:@"2" ]) {
        [self loadMasterView:@"AR"];
    }
    
}

-(void) loadMasterView:(NSString *) masterView {
    if (masterView == nil) {
        masterView = @"DefectList";
    }
    UIStoryboard* sidebarStoryboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    UIViewController *rearVC = [sidebarStoryboard instantiateViewControllerWithIdentifier:@"DefectMenu"];
    UIViewController *frontController = [sidebarStoryboard instantiateViewControllerWithIdentifier:masterView];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:frontController];
    
    [FlatTheme styleNavigationBarWithFontName:@"Avenir" andColor:[UIColor colorWithRed:222.0/255 green:59.0/255 blue:47.0/255 alpha:1.0f]];
    
    self.contentViewController = nav;
    self.sidebarViewController = rearVC;
    
    UIButton* menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 28, 20)];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* menuItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    frontController.navigationItem.leftBarButtonItem = menuItem;
    
    UIButton* addButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [addButton setBackgroundImage:[UIImage imageNamed:@"icon-plus.png"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* addItem = [[UIBarButtonItem alloc] initWithCustomView:addButton];
    
    frontController.navigationItem.rightBarButtonItem = addItem;
    
    [frontController setTitle:@"Defects list"];


}

- (void)revealToggle:(id)sender {
    
    [super toggleSidebar:!self.sidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
}

- (void)revealGesture:(UIPanGestureRecognizer *)recognizer {
    
    [super dragContentView:recognizer];
}

-(void)toggleMenuDelegate:(NSNotification *)notif {
    
    [self revealToggle:nil];
}

@end
