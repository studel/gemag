//
//  ARVC.h
//  gemag
//
//  Created by Guillaume BARANNE on 14/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AwesomeMenu.h" 

@interface ARVC : UIViewController <AwesomeMenuDelegate>

@property (nonatomic, weak) AwesomeMenu *menu;

@end
