//
//  switchMenu.h
//  gemag
//
//  Created by Guillaume BARANNE on 23/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AwesomeMenu.h"

@interface switchMenu : UIMenuController <AwesomeMenuDelegate>

@property (nonatomic, weak) AwesomeMenu *menu;
- (AwesomeMenu *) getMenu;

@end
