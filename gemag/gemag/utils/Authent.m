//
//  Authent.m
//  gemag
//
//  Created by Sylvain Tudela on 12/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "Authent.h"

@implementation Authent

//Génération du singleton
+ (id)getInstance
{
    static dispatch_once_t pred = 0;
    __strong static id authent = nil;
    dispatch_once(&pred, ^{
        authent = [[self alloc] init]; // or some other init method
    });
    
    return authent;
}

@end