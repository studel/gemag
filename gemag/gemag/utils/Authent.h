//
//  Authent.h
//  gemag
//
//  Created by Sylvain Tudela on 12/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Authent : NSObject

@property (strong, nonatomic) NSString *login;
@property (strong, nonatomic) NSString *password;

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *company;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phone;

@property (assign, nonatomic) BOOL isAuthent;

#pragma mark - Singleton

/** Création du singleton
 */
+ (id)getInstance;

@end
