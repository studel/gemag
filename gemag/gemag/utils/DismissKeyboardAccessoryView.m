//
//  DismissKeyboardAccessoryView.m
//  MAIL
//
//  Created by Alexandre on 02/01/13.
//
//

#import "DismissKeyboardAccessoryView.h"

@interface DismissKeyboardAccessoryView()

@property (nonatomic, strong) UIButton *dismissButton;

@end


@implementation DismissKeyboardAccessoryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        // on place le bouton tout à droite de la vue (lorsqu'une vue est définie comme inputAccessoryView, elle a la même largeur que le clavier)
        self.dismissButton.frame = CGRectMake(self.frame.size.width - 60, 0.0, 60.0, 30.0);
        
        // on définit la marge gauche comme flexible pour que le bouton soit toujours à droite après une rotation
        self.dismissButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        
        [self.dismissButton setImage:[UIImage imageNamed:@"dismissKeyboard.png"] forState:UIControlStateNormal];
        [self.dismissButton addTarget:self action:@selector(dismissButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.dismissButton];
    }
    return self;
}

- (void)dismissButtonPressed
{
    [self.delegate keyboardDismissButtonTapped];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    // On fait en sorte de rendre seulement bouton comme "interactif"
    // Cela permet de ne pas bloquer les appuis sur la vue qui se trouve en dessous lorsque que l'on appuie à gauche du bouton (car la vue prend toute la largeur du clavier)
    return CGRectContainsPoint(self.dismissButton.frame, point);
}

@end
