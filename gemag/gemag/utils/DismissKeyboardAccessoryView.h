//
//  DismissKeyboardAccessoryView.h
//  MAIL
//
//  Created by Alexandre on 02/01/13.
//
//

#import <UIKit/UIKit.h>

@protocol DismissKeyboardAccessoryViewDelegate;

/**
 Vue qui contient un bouton permettant de masquer le clavier.
 Cette vue est conçue pour être définie comme le `inputAccessoryView` d'un champ texte, afin d'ajouter un
 bouton juste au desssus du clavier permettant de masquer celui-ci
 */
@interface DismissKeyboardAccessoryView : UIView

@property (nonatomic, weak) id <DismissKeyboardAccessoryViewDelegate> delegate;

@end


@protocol DismissKeyboardAccessoryViewDelegate

/**
 Méthode appelée lorsque l'utilisateur appuie sur le bouton pour masquer le clavier.
 Le plus souvent, elle se contente d'appeler `resignFirstResponder` ou `setEditing:NO`
 */
- (void)keyboardDismissButtonTapped;

@end