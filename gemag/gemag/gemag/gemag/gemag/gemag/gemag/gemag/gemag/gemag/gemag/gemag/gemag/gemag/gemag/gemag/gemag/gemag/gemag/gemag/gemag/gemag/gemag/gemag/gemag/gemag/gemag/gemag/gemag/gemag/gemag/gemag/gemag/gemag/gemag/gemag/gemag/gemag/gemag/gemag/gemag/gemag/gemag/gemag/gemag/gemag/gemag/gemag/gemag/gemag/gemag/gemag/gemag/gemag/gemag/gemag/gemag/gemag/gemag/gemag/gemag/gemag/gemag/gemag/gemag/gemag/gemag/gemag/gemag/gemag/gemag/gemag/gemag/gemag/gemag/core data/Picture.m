//
//  Picture.m
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "Picture.h"
#import "Defect.h"


@implementation Picture

@dynamic name;
@dynamic defect;

@end
