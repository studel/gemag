//
//  Picture.h
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Defect;

@interface Picture : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Defect *defect;

@end
