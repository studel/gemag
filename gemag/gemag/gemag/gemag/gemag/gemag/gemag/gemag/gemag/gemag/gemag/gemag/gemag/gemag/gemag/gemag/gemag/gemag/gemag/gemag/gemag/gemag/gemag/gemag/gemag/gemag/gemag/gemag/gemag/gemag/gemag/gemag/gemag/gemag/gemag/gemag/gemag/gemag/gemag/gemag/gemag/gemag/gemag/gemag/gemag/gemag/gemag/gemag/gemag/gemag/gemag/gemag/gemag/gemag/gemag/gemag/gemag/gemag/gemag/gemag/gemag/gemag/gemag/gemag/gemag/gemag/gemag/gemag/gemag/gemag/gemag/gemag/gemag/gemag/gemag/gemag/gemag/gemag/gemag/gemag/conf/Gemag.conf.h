//
//  Gemag.conf.h
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

typedef enum {
    DEFECT_PRIORITY_LOW = 0,
    DEFECT_PRIORITY_MEDIUM = 1,
    DEFECT_PRIORITY_HIGH = 2,
    DEFECT_PRIORITY_CRITICAL = 3
} DEFECT_PRIORITY;

typedef enum {
    DEFECT_STATE_NEW = 0,
    DEFECT_STATE_SCHEDULED = 1,
    DEFECT_STATE_INCORRECTION = 2,
    DEFECT_STATE_TERMINATED = 3,
    DEFECT_STATE_PENDING = 4
} DEFECT_STATE;


#define MANAGED_OBJECT_CONTEXT_MUTEX [UIApplication sharedApplication]
