//
//  Defect.m
//  gemag
//
//  Created by Sylvain Tudela on 20/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "Defect.h"
#import "Event.h"
#import "Picture.h"


@implementation Defect

@dynamic author;
@dynamic date;
@dynamic desc;
@dynamic locationAlt;
@dynamic locationDesc;
@dynamic locationLat;
@dynamic locationLong;
@dynamic priority;
@dynamic ref;
@dynamic state;
@dynamic title;
@dynamic events;
@dynamic pictures;

@end
