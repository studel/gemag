//
//  Defect+Helper.h
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "Defect.h"
#import "CoreDataHelper.h"

@interface Defect (Helper)

+ (void)populateDefectTable;
+ (NSMutableArray *) getDefectsWithFilter:(NSDictionary*) filter andOrder:(NSString *) order andAscending:(BOOL) ascending;

@end
