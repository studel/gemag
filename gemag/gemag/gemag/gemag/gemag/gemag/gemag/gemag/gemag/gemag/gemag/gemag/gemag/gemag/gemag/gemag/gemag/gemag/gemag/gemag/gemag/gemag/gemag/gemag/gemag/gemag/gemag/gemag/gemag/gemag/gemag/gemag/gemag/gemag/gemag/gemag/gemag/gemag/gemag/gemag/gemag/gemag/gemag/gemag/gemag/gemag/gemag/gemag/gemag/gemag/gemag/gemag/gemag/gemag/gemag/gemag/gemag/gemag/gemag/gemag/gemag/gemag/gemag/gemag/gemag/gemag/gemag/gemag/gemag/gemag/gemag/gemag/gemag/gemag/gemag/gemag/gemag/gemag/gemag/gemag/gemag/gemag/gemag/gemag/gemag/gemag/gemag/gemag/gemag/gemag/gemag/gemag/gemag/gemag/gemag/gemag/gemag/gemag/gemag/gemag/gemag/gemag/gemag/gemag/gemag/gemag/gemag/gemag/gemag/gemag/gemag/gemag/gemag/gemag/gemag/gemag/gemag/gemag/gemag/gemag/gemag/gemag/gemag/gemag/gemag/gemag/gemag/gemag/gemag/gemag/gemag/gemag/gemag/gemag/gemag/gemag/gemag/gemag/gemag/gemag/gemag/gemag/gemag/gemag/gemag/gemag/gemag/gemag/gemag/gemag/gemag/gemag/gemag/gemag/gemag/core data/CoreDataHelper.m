//
//  CoreDataHelper.m
//  MAIL
//
//  Created by Maxime Margalet on 27/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CoreDataHelper.h"

@implementation CoreDataHelper

#pragma mark - Retrieve objects

// Fetch objects with a predicate
+(NSMutableArray *)searchObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate *)predicate andSortKey:(NSString*)sortKey andSortAscending:(BOOL)sortAscending andContext:(NSManagedObjectContext *)managedObjectContext
{
    @synchronized(MANAGED_OBJECT_CONTEXT_MUTEX) {
        
        //DLog(@"%s entity:%@", __func__, entityName);
        
        NSMutableArray *mutableFetchResults = nil;
        
        // Create fetch request
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
        [request setEntity:entity];
        
        // If a predicate was specified then use it in the request
        if (predicate != nil)
            [request setPredicate:predicate];
        
        // If a sort key was passed then use it in the request
        if (sortKey != nil) {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:sortAscending];
            NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
            [request setSortDescriptors:sortDescriptors];
        }
        
        // Execute the fetch request
        NSError *error = nil;
        mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
        
        // If the returned array was nil then there was an error
        if (mutableFetchResults == nil)
            DLog(@"Couldn't get objects for entity %@", entityName);
        
        // Return the results
        return mutableFetchResults;
    }
}

// Fetch objects without a predicate
+(NSMutableArray *)getObjectsForEntity:(NSString*)entityName withSortKey:(NSString*)sortKey andSortAscending:(BOOL)sortAscending andContext:(NSManagedObjectContext *)managedObjectContext
{
    return [self searchObjectsForEntity:entityName withPredicate:nil andSortKey:sortKey andSortAscending:sortAscending andContext:managedObjectContext];
}

#pragma mark - Count objects

// Get a count for an entity with a predicate
+(NSUInteger)countForEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate andContext:(NSManagedObjectContext *)managedObjectContext
{
    @synchronized(MANAGED_OBJECT_CONTEXT_MUTEX) {
        DLog(@"%s entity:%@", __func__, entityName);
        
        // Create fetch request
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
        [request setEntity:entity];
        [request setIncludesPropertyValues:NO];
        
        // If a predicate was specified then use it in the request
        if (predicate != nil)
            [request setPredicate:predicate];
        
        // Execute the count request
        NSError *error = nil;
        NSUInteger count = [managedObjectContext countForFetchRequest:request error:&error];
        
        // If the count returned NSNotFound there was an error
        if (count == NSNotFound)
            DLog(@"Couldn't get count for entity %@", entityName);
        
        // Return the results
        return count;
    }
}

// Get a count for an entity without a predicate
+(NSUInteger)countForEntity:(NSString *)entityName andContext:(NSManagedObjectContext *)managedObjectContext
{
    DLog(@"%s entity:%@", __func__, entityName);
    
    return [self countForEntity:entityName withPredicate:nil andContext:managedObjectContext];
}

#pragma mark - Delete Objects

// Delete all objects for a given entity
+(BOOL)deleteAllObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate*)predicate andContext:(NSManagedObjectContext *)managedObjectContext andSortKey:(NSString*)sortKey andSortAscending:(BOOL)sortAscending startLimit:(int)startLimit endLimit:(int)endLimit
{
    @synchronized(MANAGED_OBJECT_CONTEXT_MUTEX) {
        DLog(@"%s entity:%@", __func__, entityName);

        int cpt = 0;
        // Create fetch request
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
        [request setEntity:entity];
        
        // Ignore property values for maximum performance
        //	[request setIncludesPropertyValues:NO];
        
        
        // If a sort key was passed then use it in the request
        if (sortKey != nil) {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:sortAscending];
            NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
            [request setSortDescriptors:sortDescriptors];
        }
        
        
        // If a predicate was specified then use it in the request
        if (predicate != nil)
            [request setPredicate:predicate];
        
        // Execute the count request
        NSError *error = nil;
        NSArray *fetchResults = [managedObjectContext executeFetchRequest:request error:&error];
        
        // Delete the objects returned if the results weren't nil
        if (fetchResults != nil) {
            for (NSManagedObject *manObj in fetchResults) {
                // stupid ios doen't know SQL LIMIT ...
                if ((!startLimit && !endLimit) || (startLimit >=0 && endLimit && cpt >= startLimit && cpt + startLimit < endLimit)){
                    //Suppression du cache
                    [managedObjectContext deleteObject:manObj];
                }
                cpt++;
            }
        } else {
            DLog(@"Couldn't delete objects for entity %@", entityName);
            return NO;
        }
        
        return YES;
    }
}

+(BOOL)deleteAllObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate*)predicate andContext:(NSManagedObjectContext *)managedObjectContext startLimit:(int)startLimit endLimit:(int)endLimit{

    DLog(@"%s entity:%@", __func__, entityName);
    return [self deleteAllObjectsForEntity:entityName withPredicate:predicate andContext:managedObjectContext andSortKey:NULL andSortAscending:NO startLimit:startLimit endLimit:endLimit];
}

+(BOOL)deleteAllObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate*)predicate andContext:(NSManagedObjectContext *)managedObjectContext
{
    DLog(@"%s entity:%@", __func__, entityName);
    return [self deleteAllObjectsForEntity:entityName withPredicate:predicate andContext:managedObjectContext andSortKey:NULL andSortAscending:NO startLimit:0 endLimit:0];
}

+(BOOL)deleteAllObjectsForEntity:(NSString*)entityName andContext:(NSManagedObjectContext *)managedObjectContext
{
    return [self deleteAllObjectsForEntity:entityName withPredicate:nil andContext:managedObjectContext];
}
@end
