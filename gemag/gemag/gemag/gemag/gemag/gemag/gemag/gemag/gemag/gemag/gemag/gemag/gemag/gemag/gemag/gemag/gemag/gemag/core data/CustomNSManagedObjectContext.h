//
//  CustomNSManagedObjectContext.h
//  MAIL
//
//  Created by Maxime Margalet on 19/09/12.
//
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (CustomNSManagedObjectContext)

- (BOOL) saveContext;

@end
