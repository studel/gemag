//
//  Event.m
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import "Event.h"
#import "Defect.h"


@implementation Event

@dynamic title;
@dynamic desc;
@dynamic author;
@dynamic date;
@dynamic defect;

@end
