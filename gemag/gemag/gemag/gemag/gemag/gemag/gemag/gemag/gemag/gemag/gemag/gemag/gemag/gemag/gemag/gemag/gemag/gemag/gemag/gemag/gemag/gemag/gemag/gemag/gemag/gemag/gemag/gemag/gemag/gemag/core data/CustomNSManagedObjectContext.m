//
//  CustomNSManagedObjectContext.m
//  MAIL
//
//  Created by Maxime Margalet on 19/09/12.
//
//

#import "CustomNSManagedObjectContext.h"

@implementation NSManagedObjectContext (CustomNSManagedObjectContext)

-(BOOL)saveContext {
    // Enregistre l'objet Core Data dans le modèle de donnée
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self;
    
    if (managedObjectContext != nil) {
        @try {
            if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
                DLog(@"Unresolved error %@, %@", error, [error userInfo]);
                return NO;
            }
            else
                return YES;
        }
        @catch (NSException *exception) {
            DLog(@"!!!!! CRASH !!!!! %s : %@", __func__, [exception description]);
            return NO;
        }
    }
    
    return NO;
}

@end
