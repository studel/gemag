//
//  Utils.h
//  gemag
//
//  Created by Sylvain Tudela on 08/08/13.
//  Copyright (c) 2013 sopra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

@property (nonatomic, strong) NSArray *formatters;
@property (nonatomic, strong) NSCalendar *gregorianCal;


+ (id)getInstance;

+(UIImage*)drawImageOfSize:(CGSize)size andColor:(UIColor*)color;

+(UIBarButtonItem*)getSearchButtonItem;

+(UIBarButtonItem*)getMenuItem;

-(NSString *) getCustomDateFormatForDate:(NSDate *)date;

@end
